#!/usr/bin/env python

"""
WSGI config for gvis_website project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
import sys

## GETTING-STARTED: make sure the next line points to your settings.py:
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gvis_website.settings.prod")
## GETTING-STARTED: make sure the next line points to your django project dir:
sys.path.append(os.path.join(os.environ['OPENSHIFT_REPO_DIR'], 'gvis_website'))
from distutils.sysconfig import get_python_lib
os.environ['PYTHON_EGG_CACHE'] = get_python_lib()

import django.core.wsgi
application = django.core.wsgi.get_wsgi_application()