#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gvis_website.settings.prod")
    if os.getenv('DJANGO_DEV_ENV') == 'True':
        os.environ['DJANGO_SETTINGS_MODULE'] = 'gvis_website.settings.dev'

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
