from django.conf.urls import url
from feedback import views


urlpatterns = [
    # url(r'^$', views.feedback_list),
    # url(r'(?P<pk>[0-9]+)/$', views.feedback_detail),
    url(r'^$', views.FeedbackView.as_view(), name='feedback'),
]