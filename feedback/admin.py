from django.contrib import admin
from .models import Feedback


class FeedbackAdmin(admin.ModelAdmin):
    field = ['sending_date', 'feedback_text']
    readonly_fields = ['sending_date', 'feedback_text']

admin.site.register(Feedback, FeedbackAdmin)
