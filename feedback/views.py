from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from feedback.models import Feedback
from feedback.serializers import FeedbackSerializer
from django.views.generic import TemplateView
from django.shortcuts import render
from django.forms import ModelForm, Textarea
from django.utils.decorators import method_decorator


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


class FeedbackForm(ModelForm):
    class Meta:
        model = Feedback
        fields = ['feedback_text', 'author_name', 'author_email']
        widgets = {
            'feedback_text': Textarea(attrs={'cols': 40, 'rows': 10}),
        }
        labels = {
            'feedback_text': '* Текст сообщения',
            'author_name': 'Ваше имя',
            'author_email': 'Адрес почты для обратной связи',
        }


class FeedbackView(TemplateView):
    template_name = 'feedback/feedback.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = FeedbackForm()
        return render(request, template_name=self.template_name, context={'form': form})

    def post(self, request, *args, **kwargs):
        if request.META['CONTENT_TYPE'] == 'application/json':
            data = JSONParser().parse(request)
            serializer = FeedbackSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return JSONResponse(serializer.data, status=201)
            return JSONResponse(serializer.errors, status=400)
        else:
            form = FeedbackForm(request.POST)
            if form.is_valid():
                form.save()
                return render(request, template_name=self.template_name)
            else:
                return render(request, template_name=self.template_name, context={'form': form})
