# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='author_email',
            field=models.EmailField(max_length=100, blank=True, verbose_name='Почтовый адрес автора'),
        ),
        migrations.AddField(
            model_name='feedback',
            name='author_name',
            field=models.CharField(max_length=100, blank=True, verbose_name='Имя автора'),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='feedback_text',
            field=models.CharField(max_length=5000, verbose_name='Текст сообщения'),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='sending_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Дата отправки'),
        ),
    ]
