from django.db import models


class Feedback(models.Model):
    feedback_text = models.CharField(verbose_name="Текст сообщения", max_length=5000)
    author_name = models.CharField(verbose_name="Имя автора", max_length=100, blank=True)
    author_email = models.EmailField(verbose_name="Почтовый адрес автора", max_length=100, blank=True)
    sending_date = models.DateTimeField(verbose_name="Дата отправки", auto_now_add=True)

    def __str__(self):
        return self.feedback_text
