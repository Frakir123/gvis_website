# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0003_auto_20150928_1145'),
    ]

    operations = [
        migrations.AddField(
            model_name='gvisdownloadlink',
            name='display',
            field=models.BooleanField(default=True),
        ),
    ]
