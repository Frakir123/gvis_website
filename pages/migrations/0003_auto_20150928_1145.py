# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0002_auto_20150928_1134'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='visitor',
            name='date_visited',
        ),
        migrations.AddField(
            model_name='linkclick',
            name='click_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
