# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GvisDownloadLink',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('download_url', models.URLField()),
                ('os_type', models.CharField(max_length=3, default='WIN', choices=[('WIN', 'Windows'), ('LIN', 'Linux')])),
                ('release_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('version_major', models.PositiveSmallIntegerField()),
                ('version_minor', models.PositiveSmallIntegerField()),
                ('version_patch', models.PositiveSmallIntegerField()),
                ('version_description', ckeditor.fields.RichTextField()),
            ],
        ),
    ]
