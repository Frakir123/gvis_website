# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0004_gvisdownloadlink_display'),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoExample',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('title', models.CharField(verbose_name='Название', max_length=200)),
                ('video_id', models.CharField(verbose_name='ID видео', max_length=20)),
                ('description', models.CharField(verbose_name='Описание', max_length=1000)),
                ('data_url', models.URLField(verbose_name='Ссылка на данные')),
            ],
        ),
    ]
