# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LinkClick',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('link', models.ForeignKey(to='pages.GvisDownloadLink')),
            ],
        ),
        migrations.CreateModel(
            name='Visitor',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('ip', models.GenericIPAddressField(null=True, blank=True)),
                ('user_agent', models.CharField(max_length=1024, blank=True)),
                ('date_visited', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.AddField(
            model_name='linkclick',
            name='visitor',
            field=models.ForeignKey(to='pages.Visitor'),
        ),
    ]
