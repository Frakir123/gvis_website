from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField

OS_TYPE_CHOICES = (
    ('WIN', 'Windows'),
    ('LIN', 'Linux'),
)


class GvisDownloadLink(models.Model):
    download_url = models.URLField(blank=False)
    os_type = models.CharField(choices=OS_TYPE_CHOICES, max_length=3, blank=False, default=OS_TYPE_CHOICES[0][0])
    release_date = models.DateTimeField(default=timezone.now, blank=False)
    version_major = models.PositiveSmallIntegerField(blank=False)
    version_minor = models.PositiveSmallIntegerField(blank=False)
    version_patch = models.PositiveSmallIntegerField(blank=False)
    version_description = RichTextField(blank=False)
    display = models.BooleanField(default=True)

    def __str__(self):
        return "ver. {}.{}.{}, {}".format(self.version_major, self.version_minor, self.version_patch,
                                                dict(OS_TYPE_CHOICES).get(self.os_type))

    def get_version(self):
        return "{}.{}.{}".format(self.version_major, self.version_minor, self.version_patch)


class Visitor(models.Model):
    ip = models.GenericIPAddressField(blank=True, null=True)
    user_agent = models.CharField(max_length=1024, blank=True)

    def __str__(self):
        return str(self.ip)



class LinkClick(models.Model):
    link = models.ForeignKey(GvisDownloadLink)
    visitor = models.ForeignKey(Visitor)
    click_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "{} clicked on {}".format(str(self.visitor), str(self.link))


class VideoExample(models.Model):
    title = models.CharField(verbose_name="Название", max_length=200)
    video_id = models.CharField(verbose_name="ID видео", max_length=20)
    description = models.CharField(verbose_name="Описание", max_length=1000)
    data_url = models.URLField(verbose_name="Ссылка на данные")

    def __str__(self):
        return str(self.title)
