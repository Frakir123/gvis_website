/*global $ */

$(document).ready(function () {
	var meta;
	"use strict";

	// get user info
	$(document).ready(function () {
		if (window.location.href.toString().indexOf("download") > -1) {
			$.ajax({
				url: "/ajax/usermeta/",
				type: "GET",
				dataType: "json",
				success: function (json) {
					meta = json;
				}
			});
		}
	});

	// count clicks
	$(".download a").click(function (event) {
		if (window.location.href.toString().indexOf("download") > -1) {
			$.ajax({
				url: "/ajax/click/",
				type: "POST",
				data: {
					"REMOTE_ADDR": meta.REMOTE_ADDR,
					"HTTP_USER_AGENT": meta.HTTP_USER_AGENT,
					"download_url": event.toElement.href
				}
			});
		}
	});

	// show/hide description
	$(".toggle").click(function () {
		var display = $(this).next(".details").toggle(100),
			toggle_text = $(this).children(".toggle-text");
		if (toggle_text.text() === "(показать)") {
			toggle_text.text("(спрятать)");
		} else {
			toggle_text.text("(показать)");
		}
	});
});