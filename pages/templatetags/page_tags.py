from django import template
from pages.models import GvisDownloadLink

register = template.Library()


@register.inclusion_tag('pages/links_list.html')
def download_links(os_type, count):
    """
    Выводит ненумерованный список ссылок на загрузку Gvis, упорядоченный по версиям от новых к старым.
    :param os_type: тип ОС, по которому будут фильтроваться ссылки
    :param count: максимальное количество ссылок
    """
    links = GvisDownloadLink.objects.filter(
        display=True, os_type=os_type
    ).order_by(
        '-version_major', '-version_minor', '-version_patch'
    )[:count]
    return {'gvis_links': links}
