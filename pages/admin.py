from django.contrib import admin
from .models import GvisDownloadLink, Visitor, LinkClick, VideoExample


admin.site.register(GvisDownloadLink)
admin.site.register(Visitor)
admin.site.register(LinkClick)
admin.site.register(VideoExample)