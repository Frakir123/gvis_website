from django.test import TestCase, Client
from pages.models import GvisDownloadLink
import json


class VersionTest(TestCase):
    fixtures = ['pages_links.json']

    def setUp(self):
        self.client = Client()

    def test_get_empty_last_version_if_empty(self):
        """
        При пустой БД сервер должен вернуть пустую строку для last_version
        """
        GvisDownloadLink.objects.all().delete()
        response = self.client.get('/version/')
        json_content = json.loads(response.content.decode())
        self.assertEqual(json_content['last_version'], '')

    def test_get_last_version(self):
        """
        Сервер должен вернуть последнюю версию в last_version
        """
        response = self.client.get('/version/')
        json_content = json.loads(response.content.decode())
        self.assertEqual(json_content['last_version'], '3.3.0')
