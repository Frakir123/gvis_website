from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.utils.decorators import method_decorator
from django.http import JsonResponse
from .models import Visitor, LinkClick, GvisDownloadLink, VideoExample
from django.shortcuts import render


class ClickView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        print("GET click captured in server")
        print("REMOTE_ADDR: " + request.META['REMOTE_ADDR'])
        print("HTTP_USER_AGENT: " + request.META['HTTP_USER_AGENT'])
        return HttpResponse(content="GET OK", content_type="text/plain")

    def post(self, request, *args, **kwargs):
        print("POST click captured in server")
        print(request.POST)
        try:
            visitor = Visitor(ip=request.POST['REMOTE_ADDR'], user_agent = request.POST['HTTP_USER_AGENT'])
            visitor.save()
            link = GvisDownloadLink.objects.get(download_url__exact=request.POST['download_url'])
            click = LinkClick(link=link, visitor=visitor)
            click.save()
            return HttpResponse(content="POST OK", content_type="text/plain")
        except Exception as ex:
            print("!!!!!!! " + str(ex))


class MetaView(View):
    def get(self, request, *args, **kwargs):
        print("metaview")
        meta = {
            'HTTP_USER_AGENT': request.META['HTTP_USER_AGENT'],
            'REMOTE_ADDR': request.META['REMOTE_ADDR'],
        }
        return JsonResponse(meta)


class VersionView(View):
    def get(self, request, *args, **kwargs):
        last_version = ''
        version = GvisDownloadLink.objects.filter(display=True).order_by('-version_major', '-version_minor', '-version_patch')[:1]
        if version:
            last_version = version[0].get_version()
        response = {
            'last_version': last_version,
            'download_url':  ("http://" + request.get_host() + '/' + "download/"),
        }
        return JsonResponse(response)


def gallery(request):
    examples = VideoExample.objects.all()
    return render(request, 'pages/gallery.html', {'examples': examples})