from setuptools import setup

setup(name='Gvis',
      version='1.0',
      description='Gvis server',
      author='Roman Karetnyy',
      author_email='skyroman92@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Django==1.8.4'],
     )
